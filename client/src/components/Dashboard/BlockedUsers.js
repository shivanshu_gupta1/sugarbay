import React from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import Container from '@material-ui/core/Container';
import Footer from './Footer';
import Header from './Header';
import axios from 'axios';
import CircularProgress from '@material-ui/core/CircularProgress';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Hidden from '@material-ui/core/Hidden';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import {
  MobileView
} from 'react-device-detect';
// URI
import { API_SERVICE } from '../../config/URI';


const useStyles = makeStyles({
  card: {
    display: 'flex',
  },
  cardDetails: {
    flex: 1,
  },
  cardMedia: {
    width: 160,
  },
});


const PeopleList = ({ people, unblock }) => {
  const classes = useStyles();
  var date = new Date(people.date);
  date = date.toDateString();
 
  return (
      <>
        <Grid item xs={12} md={6}>
          <CardActionArea component="a">
            <Card className={classes.card}>
              <div className={classes.cardDetails}>
                <CardContent>
                  <MobileView>
                    <Avatar style={{ float: 'right' }} alt="Profile" src={people.blockedUserProfile} />
                  </MobileView>
                  <Typography component="h2" variant="h5">
                    {people.blockedUserName}
                  </Typography>
                  <Typography variant="subtitle1" color="textSecondary">
                    {date}
                  </Typography>
                  <Button  onClick={() => unblock(people.blockedUserEmail)} color="primary">
                    Unblock
                   </Button>
                </CardContent>
              </div>
              <Hidden xsDown>
                <CardMedia className={classes.cardMedia} image={people.blockedUserProfile} title={people.blockedUserName} />
              </Hidden>
            </Card>
          </CardActionArea>
        </Grid>
      </>
  )
}

export default function BlockedUsers() {

  const [allpeoples, setAllpeoples] = React.useState({});
  const [loading, setLoading] = React.useState(true);


  React.useEffect(() => {
    var email = sessionStorage.getItem("email");
    axios.get(`${API_SERVICE}/api/v1/main/getallpeopleblocked/${email}`)
        .then(response => {
            setAllpeoples(response.data);
            setLoading(false);
        })
  }, []);

  const refreshBlockedUsers = () => {
    var email = sessionStorage.getItem("email");
    axios.get(`${API_SERVICE}/api/v1/main/getallpeopleblocked/${email}`)
        .then(response => {
            setAllpeoples(response.data);
            setLoading(false);
        })
  }

  const unblock = (unblockUser) => {
    var email = sessionStorage.getItem("email");
    var uploadData = {
            email,
            unblockUser,
        }
        axios.post(`${API_SERVICE}/api/v1/main/unblockuser`, uploadData)
            .then((response) => {
                refreshBlockedUsers();
            }).catch(err => console.log(err));
  }

  const showPeopleList = () => {
    return allpeoples.map(people => {
        return <PeopleList people={people} unblock={unblock} key={people._id}  />
    })
  }


  return (
    <React.Fragment>
      <CssBaseline />
      <Container maxWidth="lg">
        <Header />
        <main style={{ marginTop: '20px' }}>
            {
              loading === true ? (
                  <center style={{ marginTop: '10%' }}>
                      <CircularProgress />
                  </center>
              ) : (
                allpeoples && allpeoples.length ? (
                    <Grid container spacing={4}>
                        {showPeopleList()}
                    </Grid>
                ) : (
                    <center style={{ marginTop: '20px' }}>
                        <Typography variant="h4" gutterBottom>
                            No User Blocked
                        </Typography>
                    </center>
                )
                
              )
            }
        </main>
      </Container>
      <Footer title="Footer" description="Something here to give the footer a purpose!" />
    </React.Fragment>
  );
}