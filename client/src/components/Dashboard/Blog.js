import React from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import Container from '@material-ui/core/Container';
import MainFeaturedPost from './MainFeaturedPost';
import Footer from './Footer';
import Header from './Header';
import axios from 'axios';
import CircularProgress from '@material-ui/core/CircularProgress';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Hidden from '@material-ui/core/Hidden';
import CryptoJS from 'crypto';
import Avatar from '@material-ui/core/Avatar';
import queryString from 'query-string';
import {
  MobileView
} from 'react-device-detect';
// URI
import { SECRET_KEY, API_SERVICE } from '../../config/URI';
import { Alert, AlertTitle } from '@material-ui/lab';
import IconButton from '@material-ui/core/IconButton';
import Collapse from '@material-ui/core/Collapse';
import CloseIcon from '@material-ui/icons/Close';

const useStyles = makeStyles({
  card: {
    display: 'flex',
  },
  cardDetails: {
    flex: 1,
  },
  cardMedia: {
    width: 160,
  },
});

const mainFeaturedPost = {
  title: 'Sugarbabes find their Sugardaddy',
  description:
    "The No. 1 worldwide Place where Sugarbabes find their Sugardaddy.",
  image: 'https://lajollaballroom.com/wp-content/uploads/2017/12/limusina_hummer_rosa_disco_cava_zaragoza.jpg',
  imgText: 'The No. 1 worldwide Place where Sugarbabes find their Sugardaddy',
};

const PeopleList = ({ people }) => {
  const classes = useStyles();
  var date = new Date(people.date);
  date = date.toDateString();
  var mykey = CryptoJS.createCipher('aes-128-cbc', SECRET_KEY);
  var eE = mykey.update(people._id, 'utf8', 'hex');
  eE += mykey.final('hex');

  var aboutPeople = `${people.about}`;
  if (people.about !== '') {
    aboutPeople = aboutPeople.substring(0, 50);
  }
  return (
      <>
        <Grid item xs={12} md={6}>
          <CardActionArea component="a" href={`/profile?i=${eE}`}>
            <Card className={classes.card}>
              <div className={classes.cardDetails}>
                <CardContent>
                  <MobileView>
                    <Avatar style={{ float: 'right' }} alt="Profile" src={people.profileDownloadUrl} />
                  </MobileView>
                  <Typography component="h2" variant="h5">
                    {people.fullName}
                  </Typography>
                  {
                    people.vip ? (
                      <img src="https://img.icons8.com/fluent/28/000000/vip.png" alt="VIP" />
                    ) : null
                  }
                  <Typography variant="subtitle1" color="textSecondary">
                    {date}
                  </Typography>
                  <Typography component="p" variant="p">
                    {aboutPeople}...
                  </Typography>
                  <Typography variant="subtitle1" paragraph>
                    {people.description}
                  </Typography>
                  <Typography variant="subtitle1" color="primary">
                    View
                  </Typography>
                </CardContent>
              </div>
              <Hidden xsDown>
                <CardMedia className={classes.cardMedia} image={people.profileDownloadUrl} title={people.fullName} />
              </Hidden>
            </Card>
          </CardActionArea>
        </Grid>
      </>
  )
}

export default function Blog({ location }) {

  const [allpeoples, setAllpeoples] = React.useState({});
  const [loading, setLoading] = React.useState(true);
  const [transaction, setTransaction] = React.useState(false);
  const [open, setOpen] = React.useState(true);

  React.useEffect(() => {
    const { t } = queryString.parse(location.search);
    if (t === 'd') {
      setTransaction(true);
    }
    var email = sessionStorage.getItem("email");
    axios.get(`${API_SERVICE}/api/v1/main/getallpeople/${email}`)
        .then(response => {
            setAllpeoples(response.data);
            setLoading(false);
        })
  }, []);

  const showPeopleList = () => {
    return allpeoples.map(people => {
        return <PeopleList people={people} key={people._id}  />
    })
  }


  return (
    <React.Fragment>
      <CssBaseline />
      <Container maxWidth="lg">
        <Header setLoading={setLoading} setAllpeoples={setAllpeoples} />
        {
          transaction ? (
            <Collapse in={open}>
              <br />
              <Alert 
              action={
                <IconButton
                  aria-label="close"
                  color="inherit"
                  size="small"
                  onClick={() => {
                    setOpen(false);
                  }}
                >
                  <CloseIcon fontSize="inherit" />
                </IconButton>
              }
              severity="success">
                <AlertTitle>Success</AlertTitle>
                Your Transaction was successfully done
              </Alert>
              <br />
            </Collapse>
          ) : null
        }
        <main>
          <MainFeaturedPost post={mainFeaturedPost} />
            {
              loading === true ? (
                  <center style={{ marginTop: '10%' }}>
                      <CircularProgress />
                  </center>
              ) : (
                <Grid container spacing={4}>
                  {showPeopleList()}
                </Grid>
              )
            }
        </main>
      </Container>
      <Footer title="Footer" description="Something here to give the footer a purpose!" />
    </React.Fragment>
  );
}