import React from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import Container from '@material-ui/core/Container';
import Footer from './Footer';
import Header from './Header';
import Box from '@material-ui/core/Box';
import { useTheme } from '@material-ui/core/styles';

export default function Settings() {
    const theme = useTheme();
    
    return (
        <React.Fragment>
        <CssBaseline />
        <Container maxWidth="lg">
            <Header />
        <Box mt={6}>
            <h2 className="container">
            Account settings
            </h2>

            <div className="list-group container mt-2 mb-4">
                <a href="#" className="list-group-item list-group-item-action">
                <img src="https://img.icons8.com/fluent/48/000000/password-window.png" alt="Password"/>
                Change the Password
                </a>
                <a href="#" className="list-group-item list-group-item-action">
                <img src="https://img.icons8.com/color/48/000000/administrator-male-skin-type-7.png" alt="Deactivate"/>
                Deactivate your account
                </a>
                <a href="/editprofile" className="list-group-item list-group-item-action">
                <img src="https://img.icons8.com/color/48/000000/gender-neutral-user.png" alt="Edit Your Profile"/>
                Edit Your Profile
                </a>
                <a href="#" className="list-group-item list-group-item-action">
                <img src="https://img.icons8.com/fluent/48/000000/delete-view.png" alt="Delete"/>
                Delete your profile
                </a>
            </div>
        </Box>
        </Container>
        <Footer title="Footer" description="Something here to give the footer a purpose!" />
        </React.Fragment>
    );
}