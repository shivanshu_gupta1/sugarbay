import React from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import Container from '@material-ui/core/Container';
import MainFeaturedPost from './MainFeaturedPost';
import Footer from './Footer';
import Header from './Header';
import axios from 'axios';
import CircularProgress from '@material-ui/core/CircularProgress';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Hidden from '@material-ui/core/Hidden';
import CryptoJS from 'crypto';
import Avatar from '@material-ui/core/Avatar';
import {
  MobileView
} from 'react-device-detect';
// URI
import { SECRET_KEY, API_SERVICE } from '../../config/URI';


const useStyles = makeStyles({
  card: {
    display: 'flex',
  },
  cardDetails: {
    flex: 1,
  },
  cardMedia: {
    width: 160,
  },
});

const mainFeaturedPost = {
  title: 'My Favorite List',
  description:
    "The No. 1 worldwide Place where Sugarbabes find their Sugardaddy.",
  image: 'https://sf.co.ua/2012/wallpaper-1995501.jpg',
  imgText: 'The No. 1 worldwide Place where Sugarbabes find their Sugardaddy',
};

const PeopleList = ({ people }) => {
  const classes = useStyles();
  var date = new Date(people.date);
  date = date.toDateString();
  var mykey = CryptoJS.createCipher('aes-128-cbc', SECRET_KEY);
  var eE = mykey.update(people.favoriteUserId, 'utf8', 'hex');
  eE += mykey.final('hex');
  return (
      <>
        <Grid item xs={12} md={6}>
          <CardActionArea component="a" href={`/profile?i=${eE}`}>
            <Card className={classes.card}>
              <div className={classes.cardDetails}>
                <CardContent>
                  <MobileView>
                    <Avatar style={{ float: 'right' }} alt="Profile" src={people.favoriteUserProfile} />
                  </MobileView>
                  <Typography component="h2" variant="h5">
                    {people.favoriteUserName}
                  </Typography>
                  <Typography variant="subtitle1" color="textSecondary">
                    {date}
                  </Typography>
                  <Typography variant="subtitle1" color="primary">
                    View
                  </Typography>
                </CardContent>
              </div>
              <Hidden xsDown>
                <CardMedia className={classes.cardMedia} image={people.favoriteUserProfile} title={people.favoriteUserName} />
              </Hidden>
            </Card>
          </CardActionArea>
        </Grid>
      </>
  )
}

export default function Favorites() {

  const [allpeoples, setAllpeoples] = React.useState({});
  const [loading, setLoading] = React.useState(true);


  React.useEffect(() => {
    var email = sessionStorage.getItem("email");
    axios.get(`${API_SERVICE}/api/v1/main/allfavorites/${email}`)
        .then(response => {
            setAllpeoples(response.data);
            setLoading(false);
        })
  }, []);

  const showPeopleList = () => {
    return allpeoples.map(people => {
        return <PeopleList people={people} key={people._id}  />
    })
  }


  return (
    <React.Fragment>
      <CssBaseline />
      <Container maxWidth="lg">
        <Header />
        <main>
          <MainFeaturedPost post={mainFeaturedPost} />
            {
              loading === true ? (
                  <center style={{ marginTop: '10%' }}>
                      <CircularProgress />
                  </center>
              ) : (
                <Grid container spacing={4}>
                  {showPeopleList()}
                </Grid>
              )
            }
        </main>
      </Container>
      <Footer title="Footer" description="Something here to give the footer a purpose!" />
    </React.Fragment>
  );
}