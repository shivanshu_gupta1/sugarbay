import React from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import Container from '@material-ui/core/Container';
import Footer from './Footer';
import Header from './Header';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import MainFeaturedPost from './MainFeaturedPost';
import { API_SERVICE } from '../../config/URI';
import { auth } from "../../Firebase/index";
import axios from 'axios';

const useStyles = makeStyles(() => ({
    root: {
      flexGrow: 1,
    },
    bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
    },
    title: {
    fontSize: 28,
    },
    pos: {
    marginBottom: 12,
    },
}));



export default function Credits() {
    const classes = useStyles();
    const [credits, setCredits] = React.useState('');

    React.useEffect(() => {
        auth.onAuthStateChanged(function(user) {
          if (user) {
              axios.get(`${API_SERVICE}/api/v1/main/getusercredits/${user.email}`)
                .then(response => {
                    setCredits(response.data.credits);
                })
          } else {
              window.location.href = "/";
          }
        });
      }, []);

    const mainFeaturedPost = {
        title: `Total Credits Left ${credits}`,
        description:
          "The No. 1 worldwide Place where Sugarbabes find their Sugardaddy.",
        image: 'https://images.alphacoders.com/102/102401.jpg',
        imgText: 'The No. 1 worldwide Place where Sugarbabes find their Sugardaddy',
    };
    return (
        <React.Fragment>
        <CssBaseline />
        <Container maxWidth="lg">
            <Header />
            <MainFeaturedPost post={mainFeaturedPost} />
        <Box mt={6}>
            <Grid container spacing={3}>
                <Grid item xs={3}>
                    <Card className={classes.root} variant="outlined">
                    <CardContent className="text-center">
                        <Typography className={classes.title} style={{ color: '#000000' }} gutterBottom>
                        1000 Credits
                        </Typography>
                        <Typography variant="h5" component="h2">
                        159.99 US$
                        </Typography>
                    </CardContent>
                    <CardActions>
                        <Button size="large" style={{ textDecoration: 'none', color: '#ffffff' }} href={`/payment?p=cb`} color="secondary" variant="contained" fullWidth>Choose Package</Button>
                    </CardActions>
                    </Card>
                </Grid>
                <Grid item xs={3}>
                    <Card className={classes.root} variant="outlined">
                    <CardContent className="text-center" >
                        <Typography className={classes.title} style={{ color: '#000000' }} gutterBottom>
                        500 Credits
                        </Typography>
                        <Typography variant="h5" component="h2">
                        99.99 US$
                        </Typography>
                    </CardContent>
                    <CardActions>
                        <Button size="large" variant="contained" style={{ textDecoration: 'none', color: '#ffffff' }} href={`/payment?p=cc`} fullWidth color="secondary">Choose Package</Button>
                    </CardActions>
                    </Card>
                </Grid>
                <Grid item xs={3}>
                    <Card className={classes.root} variant="outlined">
                    <CardContent className="text-center" >
                        <Typography className={classes.title} style={{ color: '#000000' }} gutterBottom>
                        150 Credits
                        </Typography>
                        <Typography variant="h5" component="h2">
                        39.99 US$
                        </Typography>
                    </CardContent>
                    <CardActions>
                        <Button size="large" variant="contained" style={{ textDecoration: 'none', color: '#ffffff' }} href={`/payment?p=cd`} fullWidth color="secondary">Choose Package</Button>
                    </CardActions>
                    </Card>
                </Grid>
                <Grid item xs={3}>
                    <Card className={classes.root} variant="outlined">
                    <CardContent className="text-center" >
                        <Typography style={{ color: '#000000' }} className={classes.title} color="secondary" gutterBottom>
                        50 Credits
                        </Typography>
                        <Typography variant="h5" component="h2">
                        17.99 US$
                        </Typography>
                    </CardContent>
                    <CardActions>
                        <Button size="large" variant="contained" style={{ textDecoration: 'none', color: '#ffffff' }} href={`/payment?p=ce`} fullWidth color="secondary">Choose Package</Button>
                    </CardActions>
                    </Card>
                </Grid>
            </Grid>
        </Box>
        </Container>
        <Footer title="Footer" description="Something here to give the footer a purpose!" />
        </React.Fragment>
    );
}