import React from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import Container from '@material-ui/core/Container';
import Footer from './Footer';
import Header from './Header';
import Box from '@material-ui/core/Box';
import PropTypes from 'prop-types';
import SwipeableViews from 'react-swipeable-views';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import { SECRET_KEY, API_SERVICE } from '../../config/URI';
import axios from 'axios';
import IconButton from '@material-ui/core/IconButton';
import Snackbar from '@material-ui/core/Snackbar';
import CloseIcon from '@material-ui/icons/Close';
import CryptoJS from 'crypto';
import { auth } from "../../Firebase/index";

const avatar = {
    verticalAlign: 'middle',
    width: '100px',
    height: '100px',
    borderRadius: '50%'
}

function TabPanel(props) {
    const { children, value, index, ...other } = props;
  
    return (
      <div
        role="tabpanel"
        hidden={value !== index}
        id={`full-width-tabpanel-${index}`}
        aria-labelledby={`full-width-tab-${index}`}
        {...other}
      >
        {value === index && (
          <Box p={3}>
            <Typography>{children}</Typography>
          </Box>
        )}
      </div>
    );
}
  
TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
};
  
function a11yProps(index) {
    return {
      id: `full-width-tab-${index}`,
      'aria-controls': `full-width-tabpanel-${index}`,
    };
}
  
const useStyles = makeStyles((theme) => ({
    root: {
      backgroundColor: theme.palette.background.paper,
      width: 500,
    },
}));


const QuestionForYou = ({ q, responseQuestion, setResponse }) => {
    var mykey = CryptoJS.createCipher('aes-128-cbc', SECRET_KEY);
    var eE = mykey.update(q.senderuserid, 'utf8', 'hex');
    eE += mykey.final('hex');                

    return (
        <>
            <div class="card mb-2 p-2">
                <center>
                    <a href={`/userprofile?i=${eE}`}>
                        <img src={q.senderprofilePic} className="card-img-top " style={avatar} alt={q.senderusername} />
                        <h4 class="card-title">{q.senderusername}</h4>
                    </a>
                </center>
                
                {
                    q.reply === undefined || q.reply === '' ? (
                        <center>
                            <button onClick={() => responseQuestion(q._id)} className="btn btn-danger w-25 mt-2">
                                Send Kiss <i className="fas fa-kiss-wink-heart"></i>
                            </button>
                        </center>
                    ) : (
                        <center>
                            <h5 className="mt-2 mb-2">You Replied: <i style={{ fontSize: '22px' }} className="fas text-danger fa-kiss-wink-heart"></i></h5>
                        </center>
                    )
                }
                
            </div>
        </>
    )
}

const QuestionFromYou = ({ q }) => {
    var mykey = CryptoJS.createCipher('aes-128-cbc', SECRET_KEY);
    var eE = mykey.update(q.profileId, 'utf8', 'hex');
    eE += mykey.final('hex');  
    return (
        <>
            <div class="card mb-2 p-2">
                <center>
                    <a href={`/profile?i=${eE}`}>
                        <img src={q.profilePic} className="card-img-top " style={avatar} alt={q.username} />
                        <h4 class="card-title">{q.username}</h4>
                        {
                            q.kiss === 1 ? (
                                <h5 class="card-title">Kiss Send</h5>
                            ) : null
                        }
                        {
                            q.reply === '1' ? (
                                <h5 class="card-title">Replied: <i className="fas text-danger fa-kiss-wink-heart"></i></h5>
                            ) : null
                        }
                    </a>
                </center>
            </div>
        </>
    )
}


export default function Kiss() {
    const classes = useStyles();
    const theme = useTheme();
    const [value, setValue] = React.useState(0);
    const [response, setResponse] = React.useState('');
    const [questionForMe, setQuestionforMe] = React.useState({});
    const [questionFromMe, setQuestionFromMe] = React.useState({});
    const [message, setMessage] = React.useState('');
    const [openSnackbar, setopenSnackbar] = React.useState(false);

    const handleClick = () => {
        setopenSnackbar(true);
    };
    const handleCloseSnackbar = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setopenSnackbar(false);
    }

    React.useEffect(() => {
        var email = sessionStorage.getItem("email");
        axios.get(`${API_SERVICE}/api/v1/main/kissesforme/${email}`)
            .then(response => {
                setQuestionforMe(response.data);
            })

        axios.get(`${API_SERVICE}/api/v1/main/kissesfromme/${email}`)
            .then(response => {
                setQuestionFromMe(response.data);
            })
    }, []);


    const handleChange = (event, newValue) => {
        setValue(newValue);
    };
    const handleChangeIndex = (index) => {
        setValue(index);
    };

    const refreshForMe = () => {
        var email = sessionStorage.getItem("email");
        axios.get(`${API_SERVICE}/api/v1/main/kissesforme/${email}`)
            .then(response => {
                setQuestionforMe(response.data);
            })
    }

    const responseQuestion = (questionId) => {
        var uploadData = {
            questionId
        }
        axios.post(`${API_SERVICE}/api/v1/main/responsequestionkiss`, uploadData)
            .then((response) => {
                if (response.status === 200) {
                    handleClick();
                    setMessage("Kiss Send");
                } 
            }).catch(err => console.log(err));
            handleClick();
            setMessage("Kiss Send");
            setResponse('');
            refreshForMe();
    }
    
    const showQuestionForYou = () => {
        return questionForMe.map(q => {
            return <QuestionForYou q={q} key={q._id} responseQuestion={responseQuestion} setResponse={setResponse}/>
        })
    }

    const showQuestionFromYou = () => {
        return questionFromMe.map(q => {
            return <QuestionFromYou q={q} key={q._id}  />
        })
    }
    
    return (
        <React.Fragment>
        <CssBaseline />
        <Snackbar
            anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'left',
            }}
            open={openSnackbar}
            autoHideDuration={5000}
            onClose={handleCloseSnackbar}
            message={message}
            action={
            <React.Fragment>
                <IconButton size="small" aria-label="close" color="inherit" onClick={handleCloseSnackbar}>
                    <CloseIcon fontSize="small" />
                </IconButton>
            </React.Fragment>
            }
        />

        <Container maxWidth="lg">
            <Header />
        <Box mt={6}>
            <AppBar position="static" color="default">
            <Tabs
            value={value}
            onChange={handleChange}
            indicatorColor="primary"
            textColor="primary"
            variant="fullWidth"
            aria-label="full width tabs example"
            >
            <Tab label="Kisses" {...a11yProps(0)} />
            <Tab label="Kisses Sent" {...a11yProps(1)} />
            </Tabs>
            </AppBar>
            <SwipeableViews
            axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
            index={value}
            onChangeIndex={handleChangeIndex}
            >
            <TabPanel value={value} index={0} dir={theme.direction}>

            {
                questionForMe && questionForMe.length ? (
                    <>
                    {showQuestionForYou()}
                    </>
                ) : (
                    <center style={{ marginTop: '20px' }}>
                        <Typography variant="h4" gutterBottom>
                            No Kisses
                        </Typography>
                    </center>
                )
            }


            </TabPanel>
            <TabPanel value={value} index={1} dir={theme.direction}>

            {
                questionFromMe && questionFromMe.length ? (
                    <>
                    {showQuestionFromYou()}
                    </>
                ) : (
                    <center style={{ marginTop: '20px' }}>
                        <Typography variant="h4" gutterBottom>
                            No Kisses
                        </Typography>
                    </center>
                )
            }

            </TabPanel>
            </SwipeableViews>
        </Box>
        </Container>
        <Footer title="Footer" description="Something here to give the footer a purpose!" />
        </React.Fragment>
    );
}