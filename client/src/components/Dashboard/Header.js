import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import SearchIcon from '@material-ui/icons/Search';
import Typography from '@material-ui/core/Typography';
import Link from '@material-ui/core/Link';
import { auth } from "../../Firebase/index";
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import axios from 'axios';

// URI
import { API_SERVICE } from '../../config/URI';
const useStyles = makeStyles((theme) => ({
  toolbar: {
    borderBottom: `1px solid ${theme.palette.divider}`,
  },
  toolbarTitle: {
    flex: 1,
  },
  toolbarSecondary: {
    justifyContent: 'space-between',
    overflowX: 'auto',
  },
  toolbarLink: {
    padding: theme.spacing(1),
    flexShrink: 0,
  },
}));

const ages = [
  '< 140'
];

for (var i = 141; i < 222; i++) {
  ages.push(i);
}

export default function Header({ setLoading, setAllpeoples }) {
  const classes = useStyles();
  const [user, setUser] = useState({});
  const [credits, setCredits] = useState('');
  const [country, setCountry] = useState('Word Wide');
  const [fullName, setfullName] = useState('');
  const [hair, setHair] = React.useState('');
  const [bodyType, setBodyType] = React.useState('');
  const [smoker, setSmoker] = React.useState('');
  const [height, setHeight] = React.useState('');
  const [vip, setVip] = React.useState(false);

  useEffect(() => {
    auth.onAuthStateChanged(function(user) {
      if (user) {
          setUser(user);
          axios.get(`${API_SERVICE}/api/v1/main/getusercredits/${user.email}`)
            .then(response => {
                setCredits(response.data.credits);
                setVip(response.data.vip);
            })
      } else {
          window.location.href = "/";
      }
    });
  }, []);

  const logOut = () => {
    auth.signOut().then(function() {
        setUser({});
        sessionStorage.clear();
    }).catch(function(error) {
        console.log(error);
    });
  }

  const search = () => {
    if (setLoading) {
      setLoading(true);
      var email = sessionStorage.getItem("email");
      console.log(email, country);
      var uploadData = {
        email,
        country,
        fullName,
        hair,
        bodyType,
        smoker,
        height
      }
      axios.post(`${API_SERVICE}/api/v1/main/search`, uploadData)
          .then((response) => {
              if (response.status === 200) {
                  setLoading(false);
                  setAllpeoples(response.data);
                  setCountry('Word Wide');
                  setfullName('');
                  setHair('');
                  setBodyType('');
              } 
          }).catch(err => console.log(err));
    } 
  }

  return (
    <React.Fragment>
      <Toolbar className={classes.toolbar}>
        <IconButton
        data-bs-toggle="collapse" data-bs-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample"
        >
          <SearchIcon />
        </IconButton>
        <Typography
          component="h2"
          variant="h5"
          color="inherit"
          align="center"
          noWrap
          className={classes.toolbarTitle}
        >
        <span className="notranslate">
        Sugarba.be
        </span>
        </Typography>
        <Typography
          component="h6"
          variant="h6"
          color="inherit"
          noWrap
          style={{ marginRight: '10px', marginTop: '8px' }}
        >
        {
          vip ? (
            <img src="https://img.icons8.com/fluent/38/000000/vip.png" className="mb-3 mr-3" alt="VIP" />
          ) : null
        }
          <Link
              color="inherit"
              noWrap
              key='Credits'
              href='/credits'
            >
            Credits Left {credits}
          </Link>
        </Typography>
        <Avatar alt="Remy Sharp" src={user.photoURL} />
      </Toolbar>
      <Toolbar component="nav" variant="dense" className={classes.toolbarSecondary}>
          <Link
            color="inherit"
            noWrap
            key='Credits'
            variant="body2"
            href='/home'
            className={classes.toolbarLink}
          >
          Home
        </Link>
        <Link
            color="inherit"
            noWrap
            key='Credits'
            variant="body2"
            href='/credits'
            className={classes.toolbarLink}
          >
          Credits
        </Link>
        <Link
            color="inherit"
            noWrap
            key='Become VIP'
            variant="body2"
            href='/becomevip'
            className={classes.toolbarLink}
          >
          Become VIP
        </Link>
        <Link
            color="inherit"
            noWrap
            key='Blocked Users'
            variant="body2"
            href='/blockedusers'
            className={classes.toolbarLink}
          >
          Blocked Users
        </Link>
        <Link
            color="inherit"
            noWrap
            key='Blocked Users'
            variant="body2"
            href='/questions'
            className={classes.toolbarLink}
          >
          Questions
        </Link>
        <Link
            color="inherit"
            noWrap
            key='Settings'
            variant="body2"
            href='/settings'
            className={classes.toolbarLink}
          >
          Settings
        </Link>
        <Link
            color="inherit"
            noWrap
            key='Help/FAQ'
            variant="body2"
            href='/faq'
            className={classes.toolbarLink}
          >
          Help/FAQ
        </Link>
        <Link
            color="inherit"
            noWrap
            key='Tour'
            variant="body2"
            href='/unlockrequest'
            className={classes.toolbarLink}
          >
          Unlock Request
        </Link>
        <Link
            color="inherit"
            noWrap
            key='Tour'
            variant="body2"
            href='/favorites'
            className={classes.toolbarLink}
          >
          Favorites
        </Link>
        <Link
            color="inherit"
            noWrap
            key='Tour'
            variant="body2"
            href='/kiss'
            className={classes.toolbarLink}
          >
          Kiss
        </Link>
        <div id="google_translate_element"></div>
        <Button onClick={logOut} size="small" variant="outlined" color="secondary">
          Logout
        </Button>
      </Toolbar>

      <div className="collapse mt-2 mb-2" id="collapseExample">
        <div className="card card-body">
          

          <center>
            <div className="row"> 
              <div className="col">
                <div className="mb-3">
                  <label for="exampleFormControlInput1" className="form-label">Result From</label>
                  <select onChange={(event) => setCountry(event.target.value)} className="form-select" aria-label="Default select example">
                      <option selected>Select Your Option</option>
                      <option value="Europe">Europe</option>
                      <option value="Asia">Asia</option>
                      <option value="Word Wide">Word Wide</option>
                  </select>
                </div>
              </div>
              <div className="col">
                <div className="mb-3">
                  <label for="exampleFormControlInput1" class="form-label">Name</label>
                  <input type="text" onChange={(event) => setfullName(event.target.value)} class="form-control" id="exampleFormControlInput1" />
                </div>
              </div>
            </div>

            <div className="row"> 
              <div className="col">
                <div className="mb-3">
                    <label for="exampleFormControlInput1" className="form-label">Body Type</label>
                    <select onChange={(event) => setBodyType(event.target.value)} className="form-select " aria-label="Default select example">
                        <option selected>Select Your Option</option>
                        <option value="Athletic">Athletic</option>
                        <option value="Slim">Slim</option>
                        <option value="Muscular">Muscular</option>
                        <option value="A Few Extra Pound">A Few Extra Pound</option>
                    </select>
                </div>
              </div>
              <div className="col">
                <div className="mb-3">
                    <label for="exampleFormControlInput1" className="form-label">Hair Colour</label>
                    <select onChange={(event) => setHair(event.target.value)} className="form-select " aria-label="Default select example">
                        <option selected>Select Your Option</option>
                        <option value="Red">Red</option>
                        <option value="Black">Black</option>
                        <option value="White">White</option>
                        <option value="Brown">Brown</option>
                        <option value="Grey">Grey</option>
                        <option value="Light Blond">Light Blond</option>
                        <option value="Dark Blond">Dark Blond</option>
                    </select>
                </div>
              </div>
            </div>


            <div className="row"> 
              <div className="col">
                <div className="mb-3">
                    <label for="exampleFormControlInput1" className="form-label">Smoker</label>
                    <select onChange={(event) => setSmoker(event.target.value)} className="form-select " aria-label="Default select example">
                        <option selected>Select Your Option</option>
                        <option value="Yes">Yes</option>
                        <option value="No">No</option>
                        <option value="Sometimes">Sometimes</option>
                    </select>
                </div>
              </div>
              <div className="col">
                <div className="mb-3">
                    <label for="exampleFormControlInput1" className="form-label">Height</label>
                    <select onChange={(event) => setHeight(event.target.value)} className="form-select " aria-label="Default select example">
                        <option selected>Select Your Option</option>
                        {ages.map((age) => (
                            <option value={age}>{age} cm</option>
                        ))}
                    </select>
                </div>
              </div>
            </div>

            <div className="mb-1">
                <button 
                type="button"
                onClick={search}
                data-bs-toggle="collapse" data-bs-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample"
                className="btn btn-primary btn-sm w-25">Search</button>
            </div>
          </center>
          


        </div>
      </div>
    </React.Fragment>
  );
}

Header.propTypes = {
  sections: PropTypes.array,
  title: PropTypes.string,
};
