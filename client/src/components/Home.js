import React from 'react';
import { Link } from 'react-router-dom';
const Home = () => {
    const avatar = {
        'verticalAlign': 'middle',
        'width': '78px',
        'height': '78px',
        'borderRadius': '50%'
    }

    const [imageSet, setimageSet] = React.useState('');
    const IMAGES = [
        'https://res.cloudinary.com/dx9dnqzaj/image/upload/v1611383696/sugarba/download_yc6j9z.jpg',
        'https://res.cloudinary.com/dx9dnqzaj/image/upload/v1607457749/sugarba/103022466-GettyImages-183317260-1_duczvh.jpg',
        'https://res.cloudinary.com/dx9dnqzaj/image/upload/v1611383829/sugarba/download_l1ydjc.jpg',
        'https://res.cloudinary.com/dx9dnqzaj/image/upload/v1611383894/sugarba/download_mqkpgx.jpg',
        'https://res.cloudinary.com/dx9dnqzaj/image/upload/v1611384503/sugarba/download_kwy0kw.jpg',
        'https://res.cloudinary.com/dx9dnqzaj/image/upload/v1611384653/sugarba/download_rrclse.jpg',
        'https://media.gq.com/photos/55df431af83274863bb003b2/16:9/w_2560%2Cc_limit/sug-daddy.jpg',
        'https://www.singleboerse.at/b6/image/upload/ds/upload/categories/GER/sugardaddy/sugardaddy.jpg',
        'https://media.gq.com/photos/55db46b295040f5e14f4f6b8/master/w_1600%2Cc_limit/Sugardaddy-GQ-2015-01.jpg'
    ]
    
    React.useEffect(() => {
        var item = IMAGES[Math.floor(Math.random() * IMAGES.length)];
        setimageSet(item);
    }, []);

    return (
        <React.Fragment>
            <header id="home" className="header-area pt-100">
                <div className="shape header-shape-one">
                    <img src="https://res.cloudinary.com/dx9dnqzaj/image/upload/v1607433567/sugarba/shape-1_nbqaee.png" alt="shape" />
                </div> 

                <div className="shape header-shape-tow animation-one">
                    <img src="https://res.cloudinary.com/dx9dnqzaj/image/upload/v1607433561/sugarba/shape-2_zuuaqy.png" alt="shape" />
                </div> 

                <div className="shape header-shape-three animation-one">
                    <img src="https://res.cloudinary.com/dx9dnqzaj/image/upload/v1607433563/sugarba/shape-3_jxexjk.png" alt="shape" />
                </div> 

                <div className="shape header-shape-fore">
                    <img src="https://res.cloudinary.com/dx9dnqzaj/image/upload/v1607433558/sugarba/shape-4_b4xjdg.png" alt="shape" />
                </div> 

                <div className="navigation-bar">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-12">
                                <nav className="navbar navbar-expand-lg">
                                    <a className="navbar-brand" href="/">
                                        <img src="https://img.icons8.com/fluent/48/000000/filled-like.png" alt="Sugarba" /> <b className="text-dark h3">Sugarba.be</b>
                                    </a>
                                    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                        <span className="toggler-icon"></span>
                                        <span className="toggler-icon"></span>
                                        <span className="toggler-icon"></span>
                                    </button>

                                    <div className="collapse navbar-collapse" id="navbarSupportedContent">
                                        <ul id="nav" className="navbar-nav ml-auto">
                                            <li className="nav-item">
                                                <a className="page-scroll" href="/">Home</a>
                                            </li>
                                            <li className="nav-item">
                                                <a className="page-scroll" href="/aboutus">About</a>
                                            </li>
                                            <li className="nav-item">
                                                <a className="page-scroll" href="/">Contact</a>
                                            </li>
                                            <li className="nav-item">
                                                <div id="google_translate_element"></div>
                                            </li>
                                        </ul> 
                                    </div>
                                    <div className="navbar-btn ml-20 d-none d-sm-block">
                                        <a className="main-btn" href="/login"><i class="fas fa-heart"></i> Login</a>
                                    </div>
                                </nav> 
                            </div>
                        </div> 
                    </div> 
                </div>
                <div className="header-banner d-flex align-items-center">
                    <div className="container">
                        <div className="row">
                            <div className="col">
                                <div className="banner-content">
                                <div className="section-title text-center pb-20">
                                    <h2 style={{ color: '#EB3656' }} className="title wow fadeInUp">I am a ?</h2>
                                </div> 
                                <div className="row justify-content-center">
                                    <div style={{ cursor: 'pointer' }} className="col-lg-4 col-md-6 col-sm-8">
                                        <div className="single-services text-center mt-30 wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="0.4s">
                                            <div className="services-icon">
                                                <i class="fas fa-mars"></i>
                                            </div>
                                            <div className="services-content mt-15">
                                            <Link style={{ textDecoration: 'none' }} className="services-title h4" to='/signup'>Sugar Daddy</Link>
                                            </div>
                                        </div> 
                                    </div>
                                    <div style={{ cursor: 'pointer' }} className="col-lg-4 col-md-6 col-sm-8">
                                        <div className="single-services text-center mt-30 wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="0.8s">
                                            <div className="services-icon">
                                                <i class="fas fa-venus"></i>
                                            </div>
                                            <div className="services-content mt-15">
                                                <Link style={{ textDecoration: 'none' }} className="services-title h4" to='/signup'>Sugar Babe</Link>
                                            </div>
                                        </div> 
                                    </div>
                                </div> 
                                </div>
                            </div>
                        </div>
                    </div> 
                    <div className="banner-image bg_cover wow fadeInUp shadow" data-wow-duration="1.5s" data-wow-delay="0.5s" style={{ backgroundImage: `url(${imageSet})` }}></div>
                </div> 
            </header>


            <section id="service" className="services-area pt-125 pb-130 gray-bg">
                <div className="container">
                    <div className="row justify-content-center">
                        <div className="col-lg-6">
                            <div className="section-title text-center pb-20">
                                <h2 className="sub-title mb-15">Let's get started</h2>
                                <h2 className="title">I am a ?</h2>
                            </div> 
                        </div>
                    </div> 
                    <div className="row justify-content-center">
                        <div style={{ cursor: 'pointer' }} className="col-lg-4 col-md-6 col-sm-8">
                            <div className="single-services text-center mt-30 wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="0.4s">
                                <div className="services-icon">
                                    <i class="fas fa-mars"></i>
                                </div>
                                <div className="services-content mt-15">
                                <Link style={{ textDecoration: 'none' }} className="services-title h4" to='/signup'>Sugar Daddy</Link>
                                </div>
                            </div> 
                        </div>
                        <div style={{ cursor: 'pointer' }} className="col-lg-4 col-md-6 col-sm-8">
                            <div className="single-services text-center mt-30 wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="0.8s">
                                <div className="services-icon">
                                    <i class="fas fa-venus"></i>
                                </div>
                                <div className="services-content mt-15">
                                    <Link style={{ textDecoration: 'none' }} className="services-title h4" to='/signup'>Sugar Babe</Link>
                                </div>
                            </div> 
                        </div>
                    </div> 
                </div> 
            </section>

            <section id="about" class="about-area pt-80 pb-130">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="about-image mt-50 clearfix">
                                <div class="single-image float-left">
                                    <img src="https://res.cloudinary.com/dx9dnqzaj/image/upload/v1607438689/sugarba/High_resolution_wallpaper_background_ID_77701582966_fthk1o.jpg" alt="About" />
                                </div> 
                                <div data-aos="fade-right" class="about-btn">
                                    <a class="main-btn" href="/"><span>Find</span> your Love</a>
                                </div>
                                <div class="single-image image-tow float-right">
                                    <img src="https://res.cloudinary.com/dx9dnqzaj/image/upload/v1607438691/sugarba/819681_cute-girl-smiling-face-photography-wallpapers-boys-girls-hd_1920x1200_h_soejnz.jpg" alt="About" />
                                </div> 
                            </div> 
                        </div>
                        <div class="col-lg-6">
                            <div class="about-content mt-45">
                                <h4 class="about-welcome">About Us </h4>
                                <h3 class="about-title mt-10">Reasons to choose</h3>
                                <p class="mt-25">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages
                                    <br /> <br />It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                                <a class="main-btn mt-25" href="/signup">Sign Up Now</a>
                            </div> 
                        </div>
                    </div> 
                </div> 
            </section>

            
            <section id="testimonial" class="testimonial-area pt-130 pb-130">
                <div class="shape shape-one">
                    <img src="assets/images/testimonial/shape.png" alt="testimonial" />
                </div>
                <div class="shape shape-tow">
                    <img src="assets/images/testimonial/shape.png" alt="testimonial" />
                </div>
                <div class="shape shape-three">
                    <img src="assets/images/testimonial/shape.png" alt="testimonial" />
                </div>
                <div class="container">
                    <div class="testimonial-bg bg_cover pt-80 pb-80 shadow" style={{ backgroundImage: "url(https://res.cloudinary.com/dx9dnqzaj/image/upload/v1607458423/sugarba/2732871_yjhage.jpg)" }}>
                        <div class="row">
                            <div class="col-xl-4 offset-xl-7 col-lg-5 offset-lg-6 col-md-8 offset-md-2 col-sm-10 offset-sm-1">
                                <div class="testimonial-active">
                                    <div class="single-testimonial text-center">
                                        <div class="testimonial-image">
                                            <img style={avatar} src="https://newsrescue.com/wp-content/uploads/2015/04/happy-person.jpg" alt="Testimonial" />
                                            <div class="quota">
                                                <i class="lni-quotation"></i>
                                            </div>
                                        </div>
                                        <div class="testimonial-content mt-20">
                                            <p>Lorem ipsum dolor sit amet, ectetur adipiscing elit. Phasellus vel erat ces, commodo lectus eu, finibus diam. m ipsum dolor sit amet, ectetur.</p>
                                            <h5 class="testimonial-name mt-15">Person 1</h5>
                                        </div>
                                    </div> 
                                    <div class="single-testimonial text-center">
                                        <div class="testimonial-image">
                                            <img style={avatar} src="https://newsrescue.com/wp-content/uploads/2015/04/happy-person.jpg" alt="Testimonial" />
                                            <div class="quota">
                                                <i class="lni-quotation"></i>
                                            </div>
                                        </div>
                                        <div class="testimonial-content mt-20">
                                            <p>Lorem ipsum dolor sit amet, ectetur adipiscing elit. Phasellus vel erat ces, commodo lectus eu, finibus diam. m ipsum dolor sit amet, ectetur.</p>
                                            <h5 class="testimonial-name mt-15">Person 2</h5>
                                        </div>
                                    </div>
                                    <div class="single-testimonial text-center">
                                        <div class="testimonial-image">
                                            <img style={avatar} src="https://newsrescue.com/wp-content/uploads/2015/04/happy-person.jpg" alt="Testimonial" />
                                            <div class="quota">
                                                <i class="lni-quotation"></i>
                                            </div>
                                        </div>
                                        <div class="testimonial-content mt-20">
                                            <p>Lorem ipsum dolor sit amet, ectetur adipiscing elit. Phasellus vel erat ces, commodo lectus eu, finibus diam. m ipsum dolor sit amet, ectetur.</p>
                                            <h5 class="testimonial-name mt-15">Person 3</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div> 
            </section>


            <footer id="footer" class="footer-area">
                <div class="footer-widget pt-80 pb-130">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-3 col-md-4 col-sm-8">
                                <div class="footer-logo mt-50">
                                    <a href="/">
                                        <img src="https://img.icons8.com/fluent/48/000000/filled-like.png" className="w-25" alt="Sugarba" /> <b className="text-dark h3">Sugarba.be</b>
                                    </a>
                                    <ul class="footer-info">
                                        <li>
                                            <div class="single-info">
                                                <div class="info-icon">
                                                    <i class="lni-phone-handset"></i>
                                                </div>
                                                <div class="info-content">
                                                    <p>+Your Phone No.</p>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="single-info">
                                                <div class="info-icon">
                                                    <i class="lni-envelope"></i>
                                                </div>
                                                <div class="info-content">
                                                    <p>contact@yourmail.com</p>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="single-info">
                                                <div class="info-icon">
                                                    <i class="lni-map"></i>
                                                </div>
                                                <div class="info-content">
                                                    <p>Company Address</p>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                    <ul class="footer-social mt-20">
                                        <li><a href="/"><i class="lni-facebook-filled"></i></a></li>
                                        <li><a href="/"><i class="lni-twitter-original"></i></a></li>
                                        <li><a href="/"><i class="lni-google"></i></a></li>
                                        <li><a href="/"><i class="lni-instagram"></i></a></li>
                                    </ul>
                                </div> 
                            </div>
                            <div class="col-lg-3 col-md-4 col-sm-6">
                                <div class="footer-link mt-45">
                                    <div class="f-title">
                                        <h4 class="title">Essential Links</h4>
                                    </div>
                                    <ul class="mt-15">
                                        <li><a href="/aboutus">About</a></li>
                                        <li><a href="/">Contact</a></li>
                                        <li><a href="/">Support</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-4 col-sm-6">
                                <div class="footer-link mt-45">
                                    <div class="f-title">
                                        <h4 class="title">Company</h4>
                                    </div>
                                    <ul class="mt-15">
                                        <li><a href="/">Link 1</a></li>
                                        <li><a href="/">Link 2</a></li>
                                        <li><a href="/">Link 3</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div> 
                    </div>
                </div> 
            </footer>


        </React.Fragment>
    )
}

export default Home
