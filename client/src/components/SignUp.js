import React, { useState } from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import { auth } from "../Firebase/index";
import axios from 'axios';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';

// URI
import { API_SERVICE } from '../config/URI';

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
      <Link color="inherit" href="/">
        Your Website
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    '& > * + *': {
      marginTop: theme.spacing(2),
    },
  },
  paper: {
    marginTop: theme.spacing(2),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

export default function SignUp() {
  const classes = useStyles();
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [value, setValue] = React.useState('female');
  const [username, setUserName] = React.useState('');
  const [signUpBtn, setsignUpBtn] = React.useState('Sign Up');
  const [message, setMessage] = React.useState('');
  
  const handleChange = (event) => {
    setValue(event.target.value);
  };

  const [open, setOpen] = React.useState(false);
  const handleClick = () => {
    setOpen(true);
  };
  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setOpen(false);
  };

  const signUp = () => {
    auth.createUserWithEmailAndPassword(email, password)
    .then((result) => {
        setsignUpBtn("Signing Up...");
        var user = result.user;
        // Profile Picture being set by default
        const uploadData = {
          value,
          email,
          username
        }
        user.updateProfile({
            photoURL: "https://kittyinpink.co.uk/wp-content/uploads/2016/12/facebook-default-photo-male_1-1.jpg"
        })
        .then(() => {
          user.sendEmailVerification().then(function() {
            axios.post(`${API_SERVICE}/api/v1/main/usersregistration`, uploadData);
            setTimeout(() => {
              sessionStorage.setItem("email", email);
              sessionStorage.setItem("type", value);
              if (value === "Sugar Daddy") {
                window.location.href = `/completeprofile?t=d`;
              } else {
                window.location.href = `/completeprofile?t=b`;
              }
            }, 1000);
          }).catch(function(error) {
              console.log(error);
          });
        })
        .catch(err => console.log(err))
    })
    .catch(function(error) {
        if (error.code === "auth/email-already-in-use") {
          handleClick();
          setMessage("This email is already registered");
          setPassword('');
          setEmail('');
          setsignUpBtn("Sign Up");
        }
    });
  }

  return (
    <Container component="main" maxWidth="xs">
      <Snackbar open={open} autoHideDuration={6000} onClose={handleClose}>
        <Alert onClose={handleClose} severity="info">
          {message}
        </Alert>
      </Snackbar>
      <CssBaseline />
      <div className={classes.paper}>
        <a href="/">
          <img className="mb-2" src="https://img.icons8.com/fluent/48/000000/filled-like.png" alt="Sugarba" />
        </a>
        <Typography component="h1" variant="h5">
          Sign Up
        </Typography>
        <div id="google_translate_element"></div>
        <form className={classes.form} noValidate>
        <div className="mb-3">
            <label for="exampleFormControlInput1" className="form-label">User Name*</label>
            <input type="text" onChange={(event) => setUserName(event.target.value)} className="form-control form-control-lg" id="exampleFormControlInput1" />
        </div>
        <div class="mb-3">
            <FormControl component="fieldset">
                <FormLabel component="legend">I am A</FormLabel>
                <RadioGroup aria-label="gender" name="gender1" value={value} onChange={handleChange}>
                    <FormControlLabel value="Sugar Daddy" control={<Radio />} label="Sugar Daddy" />
                    <FormControlLabel value="Sugar Baby" control={<Radio />} label="Sugar Baby" />
                </RadioGroup>
            </FormControl>
        </div>
        <div class="mb-3">
            <label for="exampleFormControlInput1" class="form-label">Email address*</label>
            <input onChange={(event) => setEmail(event.target.value)} value={email} type="email" class="form-control form-control-lg" id="exampleFormControlInput1" placeholder="name@example.com" />
        </div>
        <div class="mb-3">
            <label for="exampleFormControlTextarea1" class="form-label">Password*</label>
            <input onChange={(event) => setPassword(event.target.value)} value={password} type="password" class="form-control form-control-lg" id="exampleFormControlInput1" />
        </div>
        <button onClick={signUp} class="btn mt-1 btn-block btn-lg mb-2" style={{ backgroundColor: 'rgb(255 0 127)', color: '#ffffff' }} type="button">{signUpBtn}</button>
        <Grid container>
        <Grid item>
            <Link href="/login" variant="body2">
            {"Already have an account? Sign In"}
            </Link>
        </Grid>
        </Grid>
        </form>
      </div>
      <Box mt={8}>
        <Copyright />
      </Box>
    </Container>
  );
}