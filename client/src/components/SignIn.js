import React, { useState } from 'react';
import { googleProvider, facebookProvider, auth } from "../Firebase/index";
import CssBaseline from '@material-ui/core/CssBaseline';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Button from '@material-ui/core/Button';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
      <Link color="inherit" href="https://material-ui.com/">
        Your Website
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(2),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

export default function SignIn() {
  const classes = useStyles();
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [signInBtn, setSignInBtn] = useState('Sign in with Google');
  const [open, setOpen] = React.useState(false);
  const [message, setMessage] = React.useState('');
  const handleClick = () => {
    setOpen(true);
  };
  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setOpen(false);
  };

  const signIn = () => {
    auth.signInWithPopup(googleProvider).then((user) => {
      setSignInBtn('Signing In...');
      sessionStorage.setItem("userId", user.uid);
      sessionStorage.setItem("userEmail", user.email);
      window.location.href = "/home";
    }).catch(err => console.log(err));
  }

  const signInFacebook = () => {
      auth.signInWithPopup(facebookProvider).then(function(result) {
        console.log("");
      }).catch(function(error) {
          // Handle Errors here.
          var errorCode = error.code;
          var errorMessage = error.message;
          // The email of the user's account used.
          var email = error.email;
          // The firebase.auth.AuthCredential type that was used.
          var credential = error.credential;
          // ...
          setMessage(errorMessage);
      });
  }

  const login = () => {
      auth.signInWithEmailAndPassword(email, password)
      .then(() => {
          auth.onAuthStateChanged(function(user) {
              if (user) {
                  console.log(user);
                  if (!user.emailVerified) {
                    window.location.href = "/verifyemail";
                  } else {
                    sessionStorage.setItem("userId", user.uid);
                    sessionStorage.setItem("email", user.email);
                    window.location.href = "/home";
                  }
                  
              } else {
                  console.log("No User");
              }
          });
      })
      .catch(function(error) {
          var errorCode = error.code;
          console.log(errorCode);
          if ( errorCode === "auth/wrong-password" ) {
            setPassword('');
            setEmail('');
          } else if ( errorCode === "auth/user-not-found" ) {
            setMessage("No User Found");
            setPassword('');
            setEmail('');
          }
      });
  }


  return (
    <Container component="main" maxWidth="xs">
      <Snackbar open={open} autoHideDuration={6000} onClose={handleClose}>
        <Alert onClose={handleClose} severity="info">
          {message}
        </Alert>
      </Snackbar>
      <CssBaseline />
      <div className={classes.paper}>
        <a href="/">
          <img className="mb-2" src="https://img.icons8.com/fluent/48/000000/filled-like.png" alt="Sugarba" />
        </a>
        <Typography component="h1" variant="h5">
          Sign in
        </Typography>
        <div id="google_translate_element"></div>
        <form className={classes.form} noValidate>
        <div class="mb-3">
            <label for="exampleFormControlInput1" class="form-label">Email address</label>
            <input type="email" value={email} class="form-control form-control-lg" onChange={(event) => setEmail(event.target.value)} id="exampleFormControlInput1" placeholder="name@example.com" />
        </div>
        <div class="mb-3">
            <label for="exampleFormControlTextarea1" class="form-label">Password</label>
            <input type="password" value={password} class="form-control form-control-lg" onChange={(event) => setPassword(event.target.value)} id="exampleFormControlInput1" />
        </div>
        <FormControlLabel
            control={<Checkbox value="remember" color="secondary" />}
            label="Remember me"
        />
        <button onClick={login} class="btn mt-1 btn-block btn-lg mb-2" style={{ backgroundColor: 'rgb(255 0 127)', color: '#ffffff' }} type="button">Sign In</button>
        <center>
          OR
        </center>
        <Box mt={2} mb={2}>
            <Grid container>
              <Grid item xs>
                <Button size="large" onClick={() => signIn()} fullWidth>
                  <img style={{marginRight: '6px'}} alt="Google Sign In" src="https://img.icons8.com/color/20/000000/google-logo.png"/>
                  {signInBtn}
                </Button>
                
                <Button size="large" onClick={() => signInFacebook()} fullWidth>
                  <img style={{marginRight: '6px'}} src="https://img.icons8.com/fluent/20/000000/facebook-new.png"/>
                  Sign In With Facebook
                </Button>
              </Grid>
            </Grid>
        </Box>
        <Grid container>
        <Grid item xs>
            <Link href="#" variant="body2">
            Forgot password?
            </Link>
        </Grid>
        <Grid item>
            <Link href="/signup" variant="body2">
            {"Don't have an account? Sign Up"}
            </Link>
        </Grid>
        </Grid>
        </form>
      </div>
      <Box mt={8}>
        <Copyright />
      </Box>
    </Container>
  );
}