import React, { Fragment } from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import './App.css';


// Components
import Home from './components/Home';
import SignIn from './components/SignIn';
import SignUp from './components/SignUp';
import Blog from './components/Dashboard/Blog';
import Profile from './components/Dashboard/Profile';
import Profile2 from './components/Dashboard/Profile2';
import CompleteProfile from './components/CompleteProfile';
import Payment from './components/Dashboard/Payment';
import Vip from './components/Dashboard/Vip';
import UnlockRequest from './components/Dashboard/UnlockRequest';
import Credits from './components/Dashboard/Credits';
import BlockedUsers from './components/Dashboard/BlockedUsers';
import Questions from './components/Dashboard/Questions';
import Faq from './components/Dashboard/Faq';
import Favorites from './components/Dashboard/Favorites';
import Kiss from './components/Dashboard/Kiss';
import Settings from './components/Dashboard/Settings';
import VerifyEmail from './components/VerifyEmail';
import EditProfile from './components/Dashboard/EditProfile';
import Aboutus from './components/Aboutus';
function App() {
  return (
    <Fragment>
        <Router>
          <Route path="/" exact component={Home} />
          <Route path="/login" exact component={SignIn} />
          <Route path="/signup" exact component={SignUp} />
          <Route path="/home" exact component={Blog} />
          <Route path="/profile" exact component={Profile} />
          <Route path="/userprofile"  component={Profile2} />
          <Route path="/completeprofile" exact component={CompleteProfile} />
          <Route path="/payment" exact component={Payment} />
          <Route path="/becomevip" exact component={Vip} />
          <Route path="/unlockrequest" exact component={UnlockRequest} />
          <Route path="/credits" exact component={Credits} />
          <Route path="/blockedusers" exact component={BlockedUsers} />
          <Route path="/questions" exact component={Questions} />
          <Route path="/faq" exact component={Faq} />
          <Route path="/favorites" exact component={Favorites} />
          <Route path="/kiss" exact component={Kiss} />
          <Route path="/settings" exact component={Settings} />
          <Route path="/verifyemail" exact component={VerifyEmail} />
          <Route path="/editprofile" exact component={EditProfile} />
          <Route path="/aboutus" exact component={Aboutus} />
        </Router>
    </Fragment>
  );
}

export default App;
